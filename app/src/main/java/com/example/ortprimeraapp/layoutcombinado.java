package com.example.ortprimeraapp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ortprimeraapp.helpers.LoginDatabaseAdapter;

public class layoutcombinado extends AppCompatActivity {

    private EditText etUserEmail;
    private EditText etPassword;
    public String username;
    private String password;
    String storedPassword;
    Context context = this;
    LoginDatabaseAdapter loginDataBaseAdapter;
    ImageView imgCage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layoutcombinado);

        imgCage=findViewById(R.id.imgCage);

        imgCage.setVisibility(View.GONE);
        // create the instance of Database
        loginDataBaseAdapter = new LoginDatabaseAdapter(getApplicationContext());
        //******

        ///****Borra y luego Inserta un registro en el sqlLite (solo para mostrar como se hace y poder simular login)
        loginDataBaseAdapter.open();
        loginDataBaseAdapter.deleteEntry("admin");
        loginDataBaseAdapter.close();
        loginDataBaseAdapter.insertEntry("Administrador","Prueba Android","admin","123456");
        ///***************************************

        AlertDialog alertDialog = new AlertDialog.Builder(layoutcombinado.this).create();
        alertDialog.setTitle("Aviso!");
        alertDialog.setMessage("usuario admin, y password 123456");
        alertDialog.show();

        etUserEmail = (EditText) findViewById(R.id.txtEmail);
        etPassword = (EditText) findViewById(R.id.txtPassword);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            //aca tenemos que programar que hacer cuando el boton sea presionado.
            //en este caso llamamos a otra activity utilizando un intent explicito
            @Override
            public void onClick(View view) {
                try {
                    loginDataBaseAdapter = loginDataBaseAdapter.open();
                    username = etUserEmail.getText().toString();
                    password = etPassword.getText().toString();
                    if (username.equals("") || password.equals("")) {
                        AlertDialog alertDialog = new AlertDialog.Builder(layoutcombinado.this).create();
                        alertDialog.setTitle("ALERT!");
                        alertDialog.setMessage("Fill All Fields");
                        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                imgCage.setVisibility(View.VISIBLE);
                            }
                        });
                        alertDialog.show();
                    }
                    // fetch the Password form database for respective user name
                    if (!username.equals("")) {
                        storedPassword = loginDataBaseAdapter.getSinlgeEntry(username);
                        // check if the Stored password matches with Password entered by user
                        if (password.equals(storedPassword)) {
                            AlertDialog alertDialog = new AlertDialog.Builder(layoutcombinado.this).create();
                            alertDialog.setTitle("Login correcto");
                            alertDialog.setMessage("Se obtuvo usuario y password de la base de datos");
                            alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    imgCage.setVisibility(View.GONE);
                                }
                            });
                            alertDialog.show();
                        }
                        else
                        {
                            AlertDialog alertDialog = new AlertDialog.Builder(layoutcombinado.this).create();
                            alertDialog.setTitle("ALERT!");
                            alertDialog.setMessage("Usuario o contraseña incorrecto, la contraseña es " + storedPassword);
                            alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    imgCage.setVisibility(View.VISIBLE);
                                }
                            });
                            alertDialog.show();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.e("Error", "error login");
                }

            }
        });
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        // Close The Database
        loginDataBaseAdapter.close();
    }
}
