package com.example.ortprimeraapp;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LifeCicleActivity extends AppCompatActivity {

    TextView textviewlifeCicleActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cicle);
        textviewlifeCicleActivity = findViewById(R.id.textviewlifeCicleActivity);
        textviewlifeCicleActivity.setText("Ingreso en OnCreate");

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            Toast.makeText(LifeCicleActivity.this, "OnCreate con savedInstance", Toast.LENGTH_LONG).show();
        } else {
            // Probably initialize members with default values for a new instance
            Toast.makeText(LifeCicleActivity.this, "OnCreate SIN savedInstance", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        // call the superclass method first
        super.onResume();
        textviewlifeCicleActivity.setText("Ingreso en onResume");
        Toast.makeText(LifeCicleActivity.this, "onResume", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        // call the superclass method first
        super.onStart();
        textviewlifeCicleActivity.setText("Ingreso en OnStart");
        Toast.makeText(LifeCicleActivity.this, "onStart", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStop() {
        // call the superclass method first
        super.onStop();
        Toast.makeText(LifeCicleActivity.this, "onStop", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        // call the superclass method first
        Toast.makeText(LifeCicleActivity.this, "onPause", Toast.LENGTH_LONG).show();
        super.onPause();

    }
}
