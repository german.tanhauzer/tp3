package com.example.ortprimeraapp;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;

public class linearLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Se define que layout va a utilizar este Activity
        setContentView(R.layout.activity_linear_layout);

        //vamos a definir que pasa cuando presionamos el boton
        Button boton = findViewById(R.id.button);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //aca tenemos que programar que hacer cuando el boton sea presionado.
                TextView textview = findViewById(R.id.textviewActivity2);
                textview.setText("Boton presionado " + new Date().toString());
            }
        });

        //Array que asociaremos al adaptador
        String[] array = new String[]{
                "Uno"
                , "Dos"
                , "Tres"
                , "Cuatro"
                , "Cinco"
        };
        //Creación del adaptador, vamos a escoger el layout simple_list_item_1, que los mostrara
        ListAdapter adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        //Asociamos el adaptador a la vista.
        ListView listView1 = (ListView)findViewById (R.id.listViewConAdapter);
        listView1.setAdapter(adaptador);
    }
}
